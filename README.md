# Запуск

В классе Application запускаешь метод main. 
Для запуска необходимо sdk java8 или выше. 

# Как запустить jar

Заходим в каталог с jar файлом и в консоли выполняем команду `java -jar FileName.jar`

# Database

Программа смотрит на бд heroku, что бы смотреть в нее можно настроить Datasource в IntellijIdea. Для этого необходимо проследовать в меню в File -> New -> Datasource. 
В окне настройки выбераем `postgresql` (возможно придется скачать драйвер), далее выбераем режим Only Url и берем url 
`jdbc:postgresql://ec2-54-221-251-195.compute-1.amazonaws.com:5432/dd5jar9534jrm1?user=pcdqgjakeabous&amp;password=ae72d0ef86a438129731c973d1a3d851c33562bd5453752c2c78f166c6418562&amp;ssl=true&amp;sslfactory=org.postgresql.ssl.NonValidatingFactory`
Возможно понадобиться ввести логин и пароль, которые можно взять из url. 

Совет - измените имя соединения, иначе там будет красоваться весь url. 