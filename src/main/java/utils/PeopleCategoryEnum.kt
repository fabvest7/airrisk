package utils

import java.math.BigDecimal

enum class PeopleCategoryEnum {
    KID {
        override fun calculateDabValue(cA: Double, cH: Double) =
                BigDecimal(((cA * Constants.tOutKid.toDouble() * Constants.vOutKid + cH * Constants.tInKid.toDouble() * Constants.vInKid)
                        * Constants.EF.toDouble() * Constants.EDKid.toDouble()) / (Constants.BWKid * Constants.AT * 365))
    },
    TEEN {
        override fun calculateDabValue(cA: Double, cH: Double) =
                BigDecimal(((cA * Constants.tOutTeen.toDouble() * Constants.vOutTeen + cH * Constants.tInTeen.toDouble() * Constants.vInTeen)
                        * Constants.EF.toDouble() * Constants.EDTeen.toDouble()) / (Constants.BWTeen * Constants.AT * 365))
    },
    ADULT {
        override fun calculateDabValue(cA: Double, cH: Double) =
                BigDecimal(((cA * Constants.tOutAdults * Constants.vOutAdults + cH * Constants.tInAdults * Constants.vInAdult)
                        * Constants.EF.toDouble() * Constants.EDAdult.toDouble()) / (Constants.BWAdult * Constants.AT * 365))
    },
    ALL_CATEGORY {
        override fun getAllDabValues(cA: Double, cH: Double): Triple<BigDecimal, BigDecimal, BigDecimal> {
            val kidDabValue = KID.calculateDabValue(cA, cH)
            val teenDabValue = TEEN.calculateDabValue(cA, cH)
            val adultDabValue = ADULT.calculateDabValue(cA, cH)
            return Triple(kidDabValue, teenDabValue, adultDabValue)
        }
    };

    open fun calculateDabValue(cA: Double, cH: Double): BigDecimal = BigDecimal.ZERO
    open fun getAllDabValues(cA: Double, cH: Double): Triple<BigDecimal, BigDecimal, BigDecimal> =
            Triple(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO)
}