package utils;

import model.Drugs;
import model.Report;
import model.Result;
import model.Substance;
import repository.RepoImpl;

import java.sql.SQLException;
import java.util.ArrayList;
@Deprecated
public class Formula {

        private static Double riskCarcinogens(int type, double cA, double cH){
        double result;

        switch (type){
            case 1:
                result = ((cA * Constants.tOutKid * Constants.vOutKid + cH * Constants.tInKid * Constants.vInKid)
                        * Constants.EF * Constants.EDKid) / (Constants.BWKid * Constants.AT * 365);
                break;
            case 2:
                result = ((cA * Constants.tOutTeen * Constants.vOutTeen + cH * Constants.tInTeen * Constants.vInTeen)
                        * Constants.EF * Constants.EDTeen) / (Constants.BWTeen * Constants.AT * 365);
                break;
            case 3:
                result = ((cA * Constants.tOutAdults * Constants.vOutAdults + cH * Constants.tInAdults * Constants.vInAdult)
                        * Constants.EF * Constants.EDAdult) / (Constants.BWAdult * Constants.AT * 365);
                break;
            default:
                return null;
        }

        return result;
    }

    private static double riskNoncarcinogenic(Drugs drug, double AC){
        double result;
        result = AC / drug.getRfC();
        return result;
    }

    public static void calculate(Long id){
        short danger = 0;
        boolean carcinogen;
        Report report = new Report();
        RepoImpl repo = new RepoImpl();
        Drugs drug = new Drugs();
        ArrayList<Substance> substance = new ArrayList<>();
        try {
            report = (Report) repo.getObject(Report.class, id);
            substance = repo.getSubByReport(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        for (Substance sub : substance) {
            drug = getDrugs(repo, drug, sub);
            carcinogen = drug.getCarcinogenic();
            if (carcinogen) {
                double resC = riskCarcinogens(report.getCategory(), sub.getValue(), sub.getValue());
                if (resC > 1L) {
                    danger = 1;
                }
                Result result = new Result(sub.getName(), resC, true, report);
                try {
                    repo.addObject(result);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        for (Substance sub : substance) {
            drug = getDrugs(repo, drug, sub);
            carcinogen = drug.getCarcinogenic();
            if (!carcinogen) {
                double resNC = riskNoncarcinogenic(drug, sub.getValue());
                if (resNC > 1L) {
                    danger = 1;
                }
                Result result = new Result(sub.getName(), resNC, false, report);
                try {
                    repo.addObject(result);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        try {
            report.setDanger(danger);
            repo.updateObject(report);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static Drugs getDrugs(RepoImpl repo, Drugs drug, Substance sub) {
        try {
            drug = (Drugs) repo.getObject(Drugs.class, (long) sub.getRefId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return drug;
    }
}
