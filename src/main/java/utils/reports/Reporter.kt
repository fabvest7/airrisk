package utils.reports

import model.Report
import utils.PeopleCategoryEnum

/**
 * Планируется использовать для работы с отчетами. Сохранение и выдача отчета, подсчет парамметров для отчета и так далее
 * методы
 */

interface Reporter {
    fun createReport()
    fun getReport(id: Long) : Report
    fun calculate(category: PeopleCategoryEnum)
    fun calculateAllCategory()
}