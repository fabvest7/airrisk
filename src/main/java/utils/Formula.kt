package utils

import model.Drugs
import java.math.BigDecimal

//Тут лежат все формулы для подсчета рисков

private fun calculateLADD(cA: Double, cH: Double) : BigDecimal {
    val ladd = (PeopleCategoryEnum.KID.calculateDabValue(cA, cH).multiply(BigDecimal(Constants.atKid))
                    .add(PeopleCategoryEnum.ADULT.calculateDabValue(cA, cH).multiply(BigDecimal(Constants.atTeen)))
                    .add(PeopleCategoryEnum.ADULT.calculateDabValue(cA, cH).multiply(BigDecimal(Constants.atAdult))))
    return ladd.divide(BigDecimal(Constants.AT))
}

fun calculateCarcinogens(category: PeopleCategoryEnum, cA: Double, cH: Double) : BigDecimal {
    return when (category) {
        PeopleCategoryEnum.KID -> {
            PeopleCategoryEnum.KID.calculateDabValue(cA, cH).multiply(calculateSF())
        }
        PeopleCategoryEnum.TEEN, PeopleCategoryEnum.ADULT, PeopleCategoryEnum.ALL_CATEGORY -> {
            calculateLADD(cA, cH).multiply(calculateSF())
        }
    }
}

private fun calculateSF(): BigDecimal = BigDecimal.ONE

fun calculateNonCarcinogens(AC: BigDecimal, drug: Drugs) = AC.divide(BigDecimal(drug.rfC))
